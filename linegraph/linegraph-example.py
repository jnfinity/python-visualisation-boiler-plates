# this line graph includes sample values to make it easier to see what goes where and preview it. 
# it uses matplotlib and has a title as well as labels for the x and y axis. 

from matplotlib import pyplot as plt

x = range(12)
y = [3000, 3005, 3010, 2900, 2950, 3050, 3000, 3100, 2980, 2980, 2920, 3010]
plt.plot(x, y)
plt.axis([0, 12, 2900, 3100])

plt.xlabel('this is the x label')
plt.ylabel('this is the y label')
plt.title('this is a title')

plt.show()
